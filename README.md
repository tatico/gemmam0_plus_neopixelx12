# gemmaM0_plus_neopixelx12

working example of a photography light ring make with Adafuit's Gemma M0 and Neopixelx12

prerequisites
-------------
copy those libraries from the bundle[1]
- adafruit_bus_device  
- adafruit_dotstar.mpy  
- adafruit_hid  
- adafruit_pypixelbuf.mpy  
- neopixel.mpy

[1] https://github.com/adafruit/Adafruit_CircuitPython_Bundle/releases
